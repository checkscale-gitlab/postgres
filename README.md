# Postgres
Creates a docker image containing `Postgres` with a Bandlandia database and user.

## Command Line Usage Tips:

### Build the images

    $docker build --tag registry.gitlab.com/bandlandia/postgres:PG_VESION  --tag registry.gitlab.com/bandlandia/postgres:latest .

### Push Image

    docker push registry.gitlab.com/bandlandia/postgres:10.3

    docker push registry.gitlab.com/bandlandia/postgres:latest

### Run Image

    docker run --name bandlandia_postgres -p5432:5432 -e POSTGRES_PASSWORD=admin -v bandlandia_rdb:/var/lib/postgresql/data  -d registry.gitlab.com/bandlandia/postgres:PG_VERSION

## Running PSQL
If you do not wish to install the complete `postgres` `homebrew` installation on your mac, you can use a postgres image to invoke psql client in the container to connect to another `postgres` image. Below is an example connection to the *Bandlandia* database that was started by the *Bandlandia Services* `docker-compose.yml` file.

    docker run -it --rm --network services_backend registry.gitlab.com/bandlandia/postgres:latest psql -h bandlandia_rdbs -U bandlandia
